package main

import (
	"fmt"
	"io"
	"math"
)


func main(){
var n,min,max float64
var numeri [] float64
fmt.Scan(&n)

	for {
		_,err := fmt.Scan(&n)
		if err != io.EOF {
			numeri = append(numeri,n)
		}else{
			break
		}
	}

	min,max = minmax(numeri)
	fmt.Println("min:", min)
	fmt.Println("max:", max)
	fmt.Println("media:",average(numeri))
}

func minmax(s []float64) (min,max float64){
	min,max = s[0],s[0]
	if len(s)>1 {
		mi,ma := minmax(s[1:])
		min,max = math.Min(min,mi),math.Max(max,ma)
	}
	return
}


func summate(s []float64) (sum float64){
	sum = s[0]
	if len(s)>1 {

		ret := summate(s[1:])
		sum += ret
	}
	return
}

func average(s []float64) float64{
	somma := summate(s)
	return somma/float64(len(s))
}

