package main

import "fmt"

func main()  {
	var mappa map[string]int
	mappa = make(map[string]int)

	//var stringa = "ciao"
	//_, ok := m["route"]
	/*for index,carattere := range stringa{
		//anche sulle mappe posso ciclare cosi
		fmt.Printf("%d:%c\n",index,carattere)
	}*/

	mappa["strada"] = 66
	mappa["stra"] = 1

	for key, value := range mappa{
		fmt.Println("chiave:",key,"value:",value)
	}


	delete(mappa,"stra")
	n := len(mappa)

	fmt.Println(mappa)
	fmt.Println(n)

}

