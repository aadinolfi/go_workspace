package main
import "fmt"

func main()	{

	var giorno, mese, anno int

	fmt.Printf("gg / mm / aaaa ->")
	fmt.Scan(&giorno)
	fmt.Scan(&mese)
	fmt.Scan(&anno)

	giorniInUnMese := numGiorniMese(mese)

	fmt.Println(giorniInUnMese)

	fmt.Println(isDataValida(giorno,mese))

}

func numGiorniMese(mese int) int {

	switch mese {
		case 1,3,5,7,9,11:
			return 31
		case 4,6,8,10,12:
			return 30
		case 2:
			return 28
		default:
			return 0
	}



}

 func isDataValida(gg int, mm int) int {
	 if((gg > 0 && gg <= numGiorniMese(mm))  ){
		 return 0
	 }else{
		 return 1
	 }
}


/*Giorno dell'anno
----------------

Scrivere un programmaz che chiede
all'utente due interi corrispondenti al giorno e mese di
una data e calcola il numero del giorno dell'anno
(cioe` il numero di giorni dall'inizio dell'anno).

Procedere in questo modo, sviluppando un punto alla volta

- definire una funzione main che chiede i dati
e che invochera` man mano le funzioni qui sotto.

- definire una funzione numGiorniMese(mese int) int
che restituisce il numero di giorni del mese passato
come argomento, utilizzando il costrutto switch.
Testarla su diversi input avendola invocata nel main.

- definire una funzione booleana isDataValida(gg, mm int)
che verifica se e` una data valida, sfruttando la funzione
numGiorniMese. Testarla su diversi input avendola invocata
nel main.

---

Scrivere una seconda versione lez5_giornodellanno2.go
che chieda anche l'anno, dotata di

- una funzione booleana isBisestile(anno int) per la
gestione degli anni bisestili.

La funzione numGiorniMese va aggiornata per questa seconda
versione. */
