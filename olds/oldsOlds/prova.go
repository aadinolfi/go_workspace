package main
import (
    "fmt"
     "os"
    "strconv"
)
func main(){
    if(len(os.Args) > 1){
        argsWithoutProg := os.Args[1:]
        var s int = 0
        for _,num := range argsWithoutProg{
            numInt,_ := strconv.Atoi(num)
            if s >= -1 && s <= 1{
                if numInt % 2 == 0 {
                    s++
                }else {
                    s--
                }
            }else{
                break
            }
        }

        if(s >= -1 && s <= 1){
            fmt.Println("sequenza corretta")
        }else{
            fmt.Println("sequenza errata")
        }

    }else{
        fmt.Print("Non hai inserito valori")
    }
}
