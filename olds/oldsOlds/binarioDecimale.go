package main


import "fmt"
func main() {
	var numero, indice, decimale  int
	const base = 2

	fmt.Scan(&numero)

	for indice = 0; numero>=1; indice++ {

		if( numero % 2 != 0 ) {
			decimale += ( base * indice )
		}
		numero = numero / 10;

	}
		fmt.Println(decimale)

}
