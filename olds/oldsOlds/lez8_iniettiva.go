package main
import "fmt"


func main(){
 	m := map[string]int{"uno": 1, "due": 2, "Uno": 3}
	fmt.Print(isInjective(m))
}

func isInjective (m map[string]int) bool{
	m2 := make(map[int]bool)
	for _,value := range m {
			if(!m2[value]){
				m2[value] = true
			}else{
				return false
			}
		}
		return true
	}
