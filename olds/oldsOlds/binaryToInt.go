package main

import "fmt"

func main() {
	var x string
	fmt.Scan(&x)
	binaryToInt(x)
	ris := binaryToInt(x)
	fmt.Printf(ris)
}

func binaryToInt(s string) (int, bad) {
	if len(s) == 0 {
		return 0, false
	}
	v := 0
	for c := range s {
		switch c {
		case '0':
			v = 2 * v
		case '1':
			v = 2*v + 1
		default:
			return 0, false

		}
	}
	return v, true
}
