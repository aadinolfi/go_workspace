/**
  * Input:
  *  distance            => number: the distance in meters he has already traveled in the previous days
  *  roadType            => string[]: contains the different paths the kangaroo needs to run across sequentially
  *  roadLength          => number[]: contains the length in meters of each path
  *  jumpDistance        => number[]: contains the meters of a jump in that path
  */

	/**
	 *	Input:
			- roadType: [‘highway’, ‘road’, ‘mountain’]
			- roadLength: [20, 9, 100]
			- jumpDistance: [4, 2, 1]
			- distance: 5
	 */

// Jimi in the previous days has already walked 5 meters in the highway, which is 20 meters long and where each jump allows him to advance 4 meters.
// After 4 jumps he will have passed the highway and traveled a total distance of 21 meters (5 meters + 4 jumps * 4 meters).
// So, he reached the road, which is 9 meters long and where each jump allows him to advance 2 meters.
// After another 5 jumps he will overtake the road and travel a total distance of 31 meters (21 meters + 5 jumps * 2 meters). (notice: after 4 jumps he will have jumped for a total of 30 meters and the next jump will always be 2 meters).
// Finally, he's on mountain, which is 100 meters long and where each jump allows him to advance 1 meter.
// He will do the last 91 jumps here, covering a total of 122 meters (31 meters + 91 jumps * 1 meter).


  function challengeFunction (distance, roadType, roadLength, jumpDistance) {
    // Start - Write your code below
		//he can do only 100 more jumps



		for (i = 0; i < keys.length; i++) {
			currentRoadType = roadType[i];
			currenRoadLength = roadLength[i];
    	result[currentRoadType] = currenRoadLength;
		}

		for (j = 0; i < result.length; j++) {
			if(distance >= result[i]){
				currentRoadType = result[i] 
			}

			for(k = distance ; i<result[currentRoadType])
		}

      return {
        currentRoadType: undefined,     // string
        currentDistance: undefined,     // number
      }
  }`),
