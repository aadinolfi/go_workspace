package main
import (
	"fmt"
	"os"
	"strconv"
)

func main(){
	if len(os.Args) != 3 {
		panic("argc")
	}else{
		base	,_ := strconv.Atoi(os.Args[1])
		exp		,_ := strconv.Atoi(os.Args[2])
		fmt.Println(potenza(base,exp))
	}
}

func potenza (b,e int) int{
	if e == 0 {
		return 1
	}else if (e != 1) {
		b *= potenza(b , (e-1))
		return b
	}else{
		return b
	}
}
