package main
import(
	"fmt"
)
// Implementiamo il gioco degli scacchi (una parte...)
//
// Il gioco degli scacchi ha
// - una scacchiera quadrata di 8x8 caselle,
// - 16 "pezzi" bianchi e 16 neri.
// I nomi dei pezzi e il numero per ciascun colore sono:
// U+2654 (è il codice del re in unicode)
// 1	RE
// 1	REGINA
// 2	TORRE
// 2	ALFIERE
// 2	CAVALLO
// 8	PEDONE
const(
	BIANCO	= false
	NERO	= true
	NULL = iota
	RE
	REGINA
	TORRE
	ALFIERE
	CAVALLO
	PEDONE
)


type Tipo byte
type Colore bool
type Pezzo struct {
    nome byte
    colore  Colore
}
type Casella struct {
    riga byte
    colonna rune
}
type Scacchiera map[Casella]Pezzo

func StringPezzo(p Pezzo) (t string){
	re := 0x2654
	if p.colore == NERO{
		re += 6
	}
	t = string(re +int(p.nome))
	return
}
func checkCasella(r byte, c rune) bool{
	return r>=1 && r<=8 && c>=97 && c<=104
}
func getPezzo(s Scacchiera, r byte, c rune) (p Pezzo){
	return  s[Casella{r,c}]
}
func putPezzo(s Scacchiera, p Pezzo, r byte, c rune){
	s[ Casella{r,c}] = p
	return
}
func removePezzo(s Scacchiera, r byte, c rune){
	s[ Casella{r,c}] = NULL
	return
}
func init(){
	// P P P P P P P P
	// T C A D R A C T

	// for c := 97; <= 104; ++ {
	// 	if(r == 2){
	// 		s[ Casella{r,c}] = Pezzo{6,false}
	// 	}else if(r==7)
	// 		s[ Casella{r,c}] = Pezzo{6,true}
	// 	}else if(r == 8){
	// 		s[ Casella{r,c}] = Pezzo{6,true}
	// 	}else if(r == 1){
	// 		s[ Casella{r,c}] = Pezzo{6,true}
	// 	}

	for r := 1; r <=8; r++ {
		if(r <= 2 || >=7){
			if(r < = 2){
				s[ Casella{r,c}] = Pezzo{colore:true}
				if(r == 2){
					
				}
			}else{
				s[ Casella{r,c}] = Pezzo{colore:false}
			}
		}
	}
	return
}
func printScacchiera(s Scacchiera){
	return
}
func readMossa() (r1 byte, col1 rune, r2 byte, col2 rune){
	return
}
func hline(width int) string{
	fmt.Print("\n")
	for index := 0;  index < 9*width; index++ {
		fmt.Print(string('\u2502'))
	}
	return
}
