package main
import (
	"fmt"
	"os"
	"strconv"
)

func main(){
	if len(os.Args) != 2 {
		panic("argc")
	}else{
		arg,_ := strconv.Atoi(os.Args[1])
		fmt.Println(numStretteMano(arg-1))
	}
}

func numStretteMano(n int) int{
	if(n == 0 || n == 1){
		return 0
	}else{
		return somma(n)
	}

}

func somma(x int) int{
	if(x != 0){
		x += somma(x-1)
		return x
	}else{
		return 0
	}
}
