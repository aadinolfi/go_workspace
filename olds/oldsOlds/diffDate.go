package main

import "fmt"

type data struct {
	g int
	m int
	a int
}

func main() {

	var d1, d2 data
	fmt.Println("separata da spazi")
	fmt.Print("data inizio:	gg mm aaaa ->")
	fmt.Scan(&d1.g, &d1.m, &d1.a)
	fmt.Print("data fine:	gg mm aaaa ->")
	fmt.Scan(&d2.g, &d2.m, &d2.a)
	fmt.Println(distEpoca(d2) - distEpoca(d1))

}

func isBisestile(a int) bool {
	//return (a % 4 == 0 && (a % 100  != 0 || a % 400 == 0)) //sbagliata
	return (a%4 != 0) || (a%100 == 0 && a%400 != 0)
}

func lungMese(m int, a int) int {

	switch m {
	case 1, 3, 5, 7, 9, 11:
		return 31
	case 4, 6, 8, 10, 12:
		return 30
	case 2:
		if isBisestile(a) {
			return 29
		} else {
			return 28
		}
	default:
		return 0
	}

}

func lungAnno(a int) int {
	if isBisestile(a) {
		return 366
	} else {
		return 365
	}
}

func distEpoca(d data) int { //NOTE ricorda di inserire caso < di 1970
	var s int

	for anno := 1970; anno < d.a; anno++ {
		s += lungAnno(anno)
	}

	if d.a < 1970 {
		preZero := 1970 - d.a
		for anno := 0; anno < preZero; anno++ {
			s += lungAnno(anno)
		}
	}

	for mese := 1; mese < d.m; mese++ {
		s += lungMese(mese, d.a)
	}
	s += d.g

	return s
}
