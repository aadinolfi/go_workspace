package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	// if len(os.Args) != 4 {
	// 	fmt.Println("argomenti non validi")
	// 	return
	// }

	x, _ := strconv.Atoi(os.Args[0])
	y, _ := strconv.Atoi(os.Args[2])
	var r int

	// fmt.Println(os.Args[0])
	// fmt.Println(os.Args[1])
	// fmt.Println(os.Args[2])
	// fmt.Println(os.Args[3])

	switch os.Args[1] {
	case "+":
		r = x + y
		fmt.Println(r)
	case "-":
		r = x - y
		fmt.Println(r)
	case "/":
		r = x / y
		fmt.Println(r)
	case "per":
		r = x * y
		fmt.Println(r)
	default:
		fmt.Println("Errore: inserisci 3 argomenti")
	}
	/*Esercizio per casa: tieni conto degli errori
	che l'utente potrebbe fare
	*/

}
