package main
import (
	"fmt"
	"os"
	"strconv"
)

func main(){
	if len(os.Args) != 2 {
		panic("argc")
	}else{
		arg,_ := strconv.Atoi(os.Args[1])
		fmt.Println(fattoriale(arg))
	}
}

func fattoriale (x int) int{
	var fatt = x
	if(x == 0){
		return 1
	}else if(x != 1){
		fatt *= fattoriale(x-1)
			return fatt
		}else{
			return 1
		}
	}
}
