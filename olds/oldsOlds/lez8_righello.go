package main
import (
	"fmt"
	"os"
	"strconv"
)

func main(){
	if len(os.Args) != 2 {
		panic("argc")
	}else{
		arg,_ := strconv.Atoi(os.Args[1])
		righello(arg)
	}
}

func righello(n int){

	if(n != 0){
		fmt.Printf("-")
		n--
		righello(n)
	}else{
		return
	}
}

func tacca(n int){
	for i := 0; i < n; i++ {
		fmt.Printf("-")
	}
}
