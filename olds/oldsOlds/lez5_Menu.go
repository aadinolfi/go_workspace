package main

import "fmt"

/*
	Scrivere un programma lez5_menu.go che mostra un menu
	con quattro scelte indicate con le lettere dalla a alla d.
	L'utente scrive la lettera corrispondente alla sua scelta
	e il programma mostra un messaggio che indica la scelta
	effettuata.
	Chiamare scelta la variabile in cui salvare l'opzione.
*/

func main() {

	var scelta string
	message := "Proposta del giorno\n" +
		"a. pizza\n" +
		"b. penne al pomodoro\n" +
		"c. cotoletta e patatine\n" +
		"d. crostata e caffè\n" +
		"ordine?"

	fmt.Printf("%s", message)
	fmt.Scan("&scelta")

	switch scelta {
	case "a":
		fmt.Print("Pizza")
	case "b":
		fmt.Print("pomodoro")
	case "c":
		fmt.Print("Patatine")
	case "d":
		fmt.Print("Caffè")

	}

}
