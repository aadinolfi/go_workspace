package main
import (
	"fmt"
//	"os"
//	"strconv"
)

func main() {
	var x,y string
	fmt.Printf("primo:")
	fmt.Scan(&x)
	fmt.Printf("secondo:")
	fmt.Scan(&y)
	fmt.Println("swapping")
	swap(&x,&y)
	 fmt.Println("primo:",x)
	 fmt.Println("secondo:",y)
}

func swap(a,b *string){
	// tmp := *a;
	// *b = *a
	// *a = tmp
	*a,*b = *b,*a //go permette di fare quello che facevo su in maniera compatta

}
