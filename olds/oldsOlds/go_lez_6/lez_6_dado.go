package main
import (
	"fmt"
	//"os"
	//"strconv"
	"math/rand"
	"time"
)

func main() {
	// Seeding with the same value results in the same random sequence each run.
	// For different numbers, seed with a different value, such as
	// time.Now().UnixNano(), which yields a constantly-changing number.
	rand.Seed(42)
	var a [7]int
	var x int

	fmt.Println("quanti lanci vuoi fare->")
	fmt.Scan(&x)

	time.Now().Unix()

	for i := 0; i < x; i++ {
		random:=rand.Intn(6) + 1 //la funzione fa da 0 a 5, con il +1 da 1 a 6
		if(false){//test
			fmt.Printf("lancio %x->(%d)|",i+1,random)
		}
		a[random]++
	}

	for i := 1; i < len(a); i++ {
		fmt.Printf("%d:%v (%.2f %%)\n",i,a[i],( float64( a[i] ) / float64(x) * 100))
	}

}
