package check

func recIsPalyn(s string) bool {
	n := len(s)
	if len(s) <= 1 {
		return true
	} else {
		if s[0] != s[len(s)-1] {
			return false
		}
		return recIsPalyn(s[1 : len(s)-1])
	}
}
