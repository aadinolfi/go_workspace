package main
import (
	"fmt"
	"os"
	"strconv"
)

func main(){
	//argsWithProg := os.Args
	argsWithoutProg := os.Args[1:]

	//arg := os.Args[0]

	/*fmt.Println(argsWithProg)
	fmt.Println(argsWithoutProg)
	fmt.Println(arg)*/

	//somma := argsWithoutProg[0]+argsWithoutProg[1]

	primo,_ := strconv.Atoi(argsWithoutProg[0])
	secondo,_ := strconv.Atoi(argsWithoutProg[1])
	somma := primo + secondo
	fmt.Println(somma)
}