// 1.0.1 pari-dispari
// Scrivere un programma Go lez2 pari dispari.go che legge un intero n e, a
//  seconda del valore di n, stampa uno dei messaggi
// n `e pari
// oppure
// n `e dispari
// Esempi di esecuzione
// numero? 4
// 4 `e pari
// numero?
// 15
// 15 `e dispari
