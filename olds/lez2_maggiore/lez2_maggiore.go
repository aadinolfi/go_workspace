// 1.0.2 maggiore
// Scrivere un programma Go lez2 maggiore.go che legge due interi e stampa il maggiore.
// Annotazioni In Go `e possibile lo scambio diretto di valori tra due variabili: a, b = b, a
// Esempi di esecuzione
// due numeri interi: -3 12
// 12 `e il maggiore
// due numeri interi: 15 5
// 15 `e il maggiore

package main
import "fmt"
func main(){
    var a,b int
    fmt.Scanf("%d %d", &a,&b)
    if a > b{
        fmt.Println(a," è il maggiore")
    }else{
        fmt.Println(b," è il maggiore")
    }
}
