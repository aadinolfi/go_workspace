package main

import(
	"fmt"
)

func main() {
	//ARRAYS
	var a[4]int
	a[0] = 1
	i := a[0]
	//i == 1
	//literal
	b := [2]string{"Penn","Taller"}
	//or
	b := []string{"Penn","Taller"}

	//SLICES
	//slice literal
	letters := []string{"a","b","c"}
	//or whit built in func func make([]T, len, cap) []T+
	var s[]byte //this is an array
	s = make([]byte,5,5)
	// s == []byte{0, 0, 0, 0, 0}

}