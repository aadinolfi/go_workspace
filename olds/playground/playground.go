package main

import "fmt"

import(
	//"fmt"
	//"math/rand"
)

func main() {
	/*istruzione var*/
	//var i int
	var i,j int = 1, 2
	var n,correct, name = 3, false, "Paolo"
	/*short asignment*/
	y := i
	k := 3
	//n,correct, name := 3,false,"Paolo"
	/*in Go assegnamento tipo differente richiede conversione esplicità*/
	p := 42
	f := float64(i)
	/*Le costanti*/
	const Pi = 3.14
	fmt.Print("ciao:",23)

}
